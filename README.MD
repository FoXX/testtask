#### Docker config
* Rename env_example > .env
* Set params in .env

#### Web config
* Set params in app/config.php

#### Up server
* $ docker-compose --env-file .env up -d
