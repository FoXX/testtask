<?php
/**
 * @return array
 * @throws Exception
 */
function getRawServices() {
	$servicesJson = file_get_contents('https://uslugi.gospmr.org/?option=com_uslugi&view=gosuslugi&task=getUslugi');
	$services = json_decode($servicesJson, true);
	if (empty($services)) {
        throw new \Exception('Empty raw services result');
    }
	return $services['ulist'];
}

/**
 * @param array $rawServices
 * @return array
 */
function getEviewServices(array $rawServices) {
	$eviewServices = [];
	foreach ($rawServices as $service) {
		if (1 !== $service['has_electronic_view']) {
			continue;
		}
		$eviewServices[] = $service;
	}
	return $eviewServices;
}

/**
 * @param array $eviewServices
 * @return array
 */
function getFullInfo(array $eviewServices) {
	$fullInfo = [];
	foreach ($eviewServices as $service) {
		// For limit requests
		// static $i = 0; if ($i >= 50) break; else $i += 1;

		$id = $service['id'];
		$apiUrl = "https://uslugi.gospmr.org/?option=com_uslugi&view=usluga&task=getUsluga&uslugaId=$id";
		$infoJson = file_get_contents($apiUrl);
		$info = json_decode($infoJson, true);
		if (empty($info)) {
			continue;
		}
		$fullInfo[] = $info;
	}
	return $fullInfo;
}

/**
 * @param string $code
 * @return array
 */
function regionCode2Name($code) {
	$regionCodes = [
		'tir' => 'Тирасполь',
		'ben' => 'Бендеры',
		'ryb' => 'Рыбница',
		'kam' => 'Каменка',
		'dub' => 'Дубоссары',
		'gri' => 'Григориополь',
		'slo' => 'Слободзея',
		'dne' => 'Днестровск',
	];
	return array_key_exists($code, $regionCodes) ? $regionCodes[$code] : null;
}

/**
 * @param array $fullInfo
 * @return array
 */
function getShortInfo(array $fullInfo) {
	$shortInfo = [];
	foreach ($fullInfo as $info) {
		$payment = $info['description']['payment'];
		$stateDuty = $info['description']['state_duty_payment'];

		$shortInfo[] = [
			'id' => $info['id'],
			'name' => $info['name'],
			'organization' => $info['organization'],

			'payment' => $payment,
			'payment_type' => $payment['payment_type'],
			'payment_amount' => empty($payment['payment_amount_coins']) ? 0 : $payment['payment_amount_coins'] / 100,
			'payment_method' => $payment['payment_method'],

			'state_duty' => $stateDuty,
			'state_duty_tax_codes' => $stateDuty['tax_codes'],
			'state_duty_account_nums' => $stateDuty['account_numbers'],

			'has_electronic_view' => $info['description']['has_electronic_view'],
		];
	}
	return $shortInfo;
}

/**
 * @param $array
 * @param $outXmlFile
 */
function saveArr2Xml($array, $outXmlFile) { // https://stackoverflow.com/a/5965940
	function arr2xml($input, &$xmlData) {
		foreach ($input as $key => $value) {
			if (is_array($value)) {
				if (is_numeric($key)){
					$key = 'item_'.$key;
				}
				$subnode = $xmlData->addChild($key);
                arr2xml($value, $subnode);
			} else {
				$xmlData->addChild("$key", htmlspecialchars("$value"));
			}
		 }
	}
	$xmlData = new \SimpleXMLElement('<?xml version="1.0"?><data></data>');
	arr2xml($array, $xmlData);
	$result = $xmlData->asXML($outXmlFile);
}

/**
 * @param $filePath
 * @param $ftpConf
 * @throws Exception
 */
function sendFileByFTP($filePath, $ftpConf) {
	if (! file_exists($filePath)) {
		throw new \Exception("FTP: File '$filePath' not exists", 1);
	}
	$ftpHost = $ftpConf['host'];
	$ftpConn = ftp_connect($ftpHost, $ftpConf['port']);
	if (empty($ftpConn)) {
        throw new Exception("FTP: Couldn't connect to $ftpHost", 1);
    }
    ftp_login($ftpConn, $ftpConf['user'], $ftpConf['pass']);
	$localFile = fopen($filePath, 'r');
	$remoteFile = basename($filePath);

	if (! ftp_fput($ftpConn, $remoteFile, $localFile)) {
		throw new \Exception("FTP: File '$filePath' not uploaded", 1);
	}
	fclose($localFile);
	ftp_close($ftpConn);
}

/**
 * @param $var
 */
function renderVar($var) {
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}

/**
 * @param string $viewFile
 * @param array $services
 * @param string html
 */
function renderView($viewFile, array $services) {
	$fullViewPath = "view/$viewFile";
	include $fullViewPath;
}
