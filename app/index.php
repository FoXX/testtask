<?php
include_once 'core.php';
include_once 'config.php';
include_once 'requestsAR.php';
global $ftpConf;
global $dbConf;

$ts = (new \DateTime)->getTimestamp();
$xmlFile = "xml/$ts.xml";
$errors = [];

try {
	// 1
	$rawServices = getRawServices();

	// 2
	$eviewServices = getEviewServices($rawServices);

	// 3
	$fullInfo = getFullInfo($eviewServices);

	// 4
	$shortInfo = getShortInfo($fullInfo);

	// 5
	saveArr2Xml($shortInfo, $xmlFile);

	// 6
	sendFileByFTP($xmlFile, $ftpConf);

	// 8
	// renderVar($shortInfo);
	renderView('template.php', $shortInfo);
} catch (\Exception $e) {
	$errors[] = [
        'file' => $e->getFile(),
        'line' => $e->getLine(),
        'err_msg' => $e->getMessage(),
        'err_code' => $e->getCode(),
    ];
    renderVar($errors);
}
// 7
$db = new RequestsAR($dbConf);
$db->saveReq2Db($ts, $errors);
$db->closeDbConnect();
