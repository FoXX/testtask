<?php
class RequestsAR
{
    const TABLE_NAME = 'requests';
    protected $dbConn = null;

    public function __construct(array $dbConf) {
        $host = $dbConf['host'];
        $port = $dbConf['port'];
        $user = $dbConf['user'];
        $pass = $dbConf['pass'];
        $dbname = $dbConf['dbname'];
        $this->dbConn = new \PDO("pgsql:host=$host;port=$port;dbname=$dbname", $user, $pass);
    }

    public function saveReq2Db($ts, array $errors = []) {
        $errorsJson = empty($errors) ? '' : json_encode($errors);
        $sql = "INSERT INTO " . self::TABLE_NAME . " (req_ts, runtime_errors) VALUES (:ts, :errors);";
        $sth = $this->dbConn->prepare($sql);
        $sth->bindParam(':ts', $ts);
        $sth->bindParam(':errors', $errorsJson);
        return $sth->execute();
    }

    public function closeDbConnect() {
        $this->dbConn = null;
    }

    public function __destruct() {
        $this->closeDbConnect();
    }
}
