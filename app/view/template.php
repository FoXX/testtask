<link rel="stylesheet" href="view/style.css">
<table border="1" cellpadding="1" cellspacing="1">
    <thead>
        <tr>
            <th id="id">id</th>
            <th id="name">name</th>
            <th id="organization">organization</th>
            <th id="payment">payment</th>
            <th id="state_duty_payment">state_duty_payment</th>
            <th id="has_eview">has_eview</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($services as $service): ?>
        <tr>
            <td><?=$service['id'] ?></td>
            <td><?=$service['name'] ?></td>
            <td><?=$service['organization'] ?></td>

            <!-- Payment -->
            <td>
                <div>
                    <span class="payment-col-title">Сумма оплаты: </span><?=$service['payment_amount'] ?>
                </div>
                <?php if (! empty($service['payment_type'])): ?>
                    <div>
                        <span class="payment-col-title">Тип оплаты: </span><?=$service['payment_type'] ?>
                    </div>
                <?php endif; ?>
            </td>

            <!-- State duty -->
            <td>
                <div>
                    <div class="duty-col-title">Налоговые коды:</div>
                    <?php foreach ($service['state_duty_tax_codes'] as $key => $num): ?>
                        <div>
                            <?php
                                $regionName = regionCode2Name(substr($key, -3));
                                if (empty($regionName)) continue;
                                echo "$regionName: $num";
                            ?>
                        </div>
                    <?php endforeach; ?>
                    <hr>

                    <div class="duty-col-title">Налоговые счета: </div>
                    <?php foreach ($service['state_duty_account_nums'] as $key => $num): ?>
                        <div>
                            <?php
                                $regionName = regionCode2Name(substr($key, -3));
                                if (empty($regionName)) continue;
                                echo "$regionName: $num";
                            ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </td>

            <td><?=$service['has_electronic_view'] ? 'yes' : 'no' ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
