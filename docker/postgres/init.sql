CREATE TABLE IF NOT EXISTS requests (
	id SERIAL PRIMARY KEY,
	req_ts TEXT NOT NULL,
	runtime_errors TEXT
);
